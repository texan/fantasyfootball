﻿using System.Collections.Generic;
using System.Linq;

namespace EspnStats
{
    public class Matchup
    {
        public int Week { get; }
        public int Year { get; }

        public string Team1Owner { get; } = "";
        public string SecondTeam1Owner { get; } = "";

        public string Team2Owner { get; } = "";
        public string SecondTeam2Owner { get; } = "";

        Dictionary<string, int> Team1Points { get; } = new Dictionary<string, int>();
        Dictionary<string, int> Team2Points { get; } = new Dictionary<string, int>();

        public Matchup(int week, int year, string sourceCode)
        {
            Week = week;
            Year = year;

            // Parse the owners
            sourceCode = sourceCode.Substring(sourceCode.IndexOf("<div class=\"teamInfoOwnerData\">") + 31);
            Team1Owner = sourceCode.Substring(0, sourceCode.IndexOf("</div>"));

            if (sourceCode.IndexOf("<div class=\"teamInfoOwnerCallouts\">") > sourceCode.IndexOf("<div class=\"teamInfoOwnerData\">"))
            {
                sourceCode = sourceCode.Substring(sourceCode.IndexOf("<div class=\"teamInfoOwnerData\">") + 31);
                SecondTeam1Owner = sourceCode.Substring(0, sourceCode.IndexOf("</div>"));
            }

            sourceCode = sourceCode.Substring(sourceCode.IndexOf("<div class=\"teamInfoOwnerData\">") + 31);
            Team2Owner = sourceCode.Substring(0, sourceCode.IndexOf("</div>"));

            if (sourceCode.IndexOf("<div class=\"teamInfoOwnerData\">") != -1)
            {
                sourceCode = sourceCode.Substring(sourceCode.IndexOf("<div class=\"teamInfoOwnerData\">") + 31);
                SecondTeam2Owner = sourceCode.Substring(0, sourceCode.IndexOf("</div>"));
            }

            // Parse the points
            List<string> positions = new List<string>();
            List<int> points = new List<int>();

            for (int players = 1; players < 19; players++)
            {
                if (((year <= 2013 && sourceCode.IndexOf(">Total") < sourceCode.IndexOf("appliedPoints")) || (year >= 2014 && (sourceCode.IndexOf("danglerBox totalScore") < sourceCode.IndexOf("ProGameFinal") || sourceCode.IndexOf("ProGameFinal") == -1))) && (sourceCode.IndexOf(">Total") != -1 || sourceCode.IndexOf("danglerBox totalScore") != -1))
                {
                    positions.Add("N/A");
                    points.Add(0);
                }
                else
                {
                    sourceCode = sourceCode.Substring(sourceCode.IndexOf("<td class=\"playertablePlayerName") + 32);
                    sourceCode = sourceCode.Substring(sourceCode.IndexOf("&nbsp;") + 6);

                    if (sourceCode.IndexOf("<") < sourceCode.IndexOf("&nbsp;") || sourceCode.IndexOf("&nbsp;") == -1)
                        positions.Add(sourceCode.Substring(0, sourceCode.IndexOf("<")));
                    else
                        positions.Add(sourceCode.Substring(0, sourceCode.IndexOf("&nbsp;")));

                    if (year <= 2013)
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("appliedPoints") + 15);

                    if (year >= 2014)
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("ProGameFinal") + 14);

                    points.Add(int.Parse(sourceCode.Substring(0, sourceCode.IndexOf("<"))));
                }

                if (players == 9)
                {
                    positions.Add("Total");

                    if (year <= 2013)
                    {
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("appliedPoints") + 15);
                        points.Add(int.Parse(sourceCode.Substring(0, sourceCode.IndexOf("</td>"))));
                    }

                    if (year >= 2014)
                    {
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("danglerBox totalScore"));
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf(">") + 1);
                        points.Add(int.Parse(sourceCode.Substring(0, sourceCode.IndexOf("</div>"))));
                    }

                    Team1Points.Add("QB",       QbPoints(positions, points));
                    Team1Points.Add("RB1",     Rb1Points(positions, points));
                    Team1Points.Add("RB2",     Rb2Points(positions, points));
                    Team1Points.Add("WR1",     Wr1Points(positions, points));
                    Team1Points.Add("WR2",     Wr2Points(positions, points));
                    Team1Points.Add("TE",       TePoints(positions, points));
                    Team1Points.Add("FLEX",   FlexPoints(positions, points));
                    Team1Points.Add("D/ST",      DPoints(positions, points));
                    Team1Points.Add("K",         KPoints(positions, points));
                    Team1Points.Add("Total", TotalPoints(positions, points));

                    positions.Clear();
                    points.Clear();

                    if (year == 2015)
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("STARTERS"));
                }

                if (players == 18)
                {
                    positions.Add("Total");

                    if (year <= 2013)
                    {
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("appliedPoints") + 15);
                        points.Add(int.Parse(sourceCode.Substring(0, sourceCode.IndexOf("</td>"))));
                    }

                    if (year >= 2014)
                    {
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf("danglerBox totalScore"));
                        sourceCode = sourceCode.Substring(sourceCode.IndexOf(">") + 1);
                        points.Add(int.Parse(sourceCode.Substring(0, sourceCode.IndexOf("</div>"))));
                    }

                    Team2Points.Add("QB", QbPoints(positions, points));
                    Team2Points.Add("RB1", Rb1Points(positions, points));
                    Team2Points.Add("RB2", Rb2Points(positions, points));
                    Team2Points.Add("WR1", Wr1Points(positions, points));
                    Team2Points.Add("WR2", Wr2Points(positions, points));
                    Team2Points.Add("TE", TePoints(positions, points));
                    Team2Points.Add("FLEX", FlexPoints(positions, points));
                    Team2Points.Add("D/ST", DPoints(positions, points));
                    Team2Points.Add("K", KPoints(positions, points));
                    Team2Points.Add("Total", TotalPoints(positions, points));

                    positions.Clear();
                    points.Clear();
                }
            }
        }

        #region Point Returns
        private int QbPoints(List<string> positions, List<int> points)
        {
            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("QB"))
                    return points[i];

            return 0;
        }

        private int Rb1Points(List<string> positions, List<int> points)
        {
            List<int> rbs = new List<int>();
            List<int> rbPoints = new List<int>();

            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("RB"))
                    rbs.Add(i);

            for (int j = 0; j < rbs.Count; j++)
                rbPoints.Add(points[rbs[j]]);

            return rbPoints.Max();
        }

        private int Rb2Points(List<string> positions, List<int> points)
        {
            List<int> rbs = new List<int>();
            List<int> rbPoints = new List<int>();

            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("RB"))
                    rbs.Add(i);

            for (int j = 0; j < rbs.Count; j++)
                rbPoints.Add(points[rbs[j]]);

            rbPoints.Remove(rbPoints.Max());
            if (rbPoints.Count != 0)
                return rbPoints.Max();

            return 0;
        }

        private int Wr1Points(List<string> positions, List<int> points)
        {
            List<int> wrs = new List<int>();
            List<int> wrPoints = new List<int>();

            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("WR"))
                    wrs.Add(i);

            for (int j = 0; j < wrs.Count; j++)
                wrPoints.Add(points[wrs[j]]);

            return wrPoints.Max();
        }

        private int Wr2Points(List<string> positions, List<int> points)
        {
            List<int> wrs = new List<int>();
            List<int> wrPoints = new List<int>();

            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("WR"))
                    wrs.Add(i);

            for (int j = 0; j < wrs.Count; j++)
                wrPoints.Add(points[wrs[j]]);

            wrPoints.Remove(wrPoints.Max());
            if (wrPoints.Count != 0)
                return wrPoints.Max();

            return 0;
        }

        private int TePoints(List<string> positions, List<int> points)
        {
            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("TE"))
                    return points[i];

            return 0;
        }

        private int FlexPoints(List<string> positions, List<int> points)
        {
            List<int> flexes = new List<int>();
            List<int> flexPoints = new List<int>();

            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("RB") || positions[i].Equals("WR"))
                    flexes.Add(i);

            for (int j = 0; j < flexes.Count; j++)
                flexPoints.Add(points[flexes[j]]);

            return flexPoints.Min();
        }

        private int DPoints(List<string> positions, List<int> points)
        {
            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("D/ST"))
                    return points[i];

            return 0;
        }

        private int KPoints(List<string> positions, List<int> points)
        {
            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("K"))
                    return points[i];

            return 0;
        }

        private int TotalPoints(List<string> positions, List<int> points)
        {
            for (int i = 0; i < 10; i++)
                if (positions[i].Equals("Total"))
                    return points[i];

            return 0;
        }
        #endregion

        private string DictionaryToCsv(Dictionary<string, int> points)
        {
            int pointValue = 0;
            string pointString = "";

            if (points.TryGetValue("QB", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("RB1", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("RB2", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("WR1", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("WR2", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("TE", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("FLEX", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("D/ST", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("K", out pointValue))
            {
                pointString += pointValue.ToString();
                pointString += ",";
            }

            if (points.TryGetValue("Total", out pointValue))
            {
                pointString += pointValue.ToString();
            }

            return pointString;
        }

        private string Result(Dictionary<string, int> team1, Dictionary<string, int> team2)
        {
            int team1Total = 0;
            int team2Total = 0;

            team1.TryGetValue("Total", out team1Total);
            team2.TryGetValue("Total", out team2Total);

            if (team1Total > team2Total)
                return "W";
            else if (team2Total > team1Total)
                return "L";
            else
                return "T";
        }

        public override string ToString() => string.Format($"{Team1Owner},{SecondTeam1Owner},{DictionaryToCsv(Team1Points)},{Result(Team1Points, Team2Points)},{Team2Owner}");
    }
}
