﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace EspnStats
{
    public class Program
    {
        static void Main(string[] args)
        {
            string url = "";
            string sourceCode = "";
            WebRequest req = null;

            Dictionary<int, Matchup> matchups = new Dictionary<int, Matchup>();
            int matchupCounter = 1;
            int numberOfTeams = 10;
            Matchup matchupToWrite = null;

            StringBuilder csv = new StringBuilder();

            for (int year = 2012; year < 2016; year++)
            {
                if (year == 2014)
                    numberOfTeams = 12;

                for (int matchupId = 1; matchupId < 15; matchupId++)
                {
                    // 2014 went 14 weeks, 13 week seasons otherwise
                    if (matchupId == 14 && year != 2014)
                        continue;

                    for (int teamId = 1; teamId < 19; teamId++)
                    {
                        url = string.Format($"http://games.espn.go.com/ffl/boxscorequick?leagueId=551448&teamId={teamId}&scoringPeriodId={matchupId}&seasonId={year}&view=scoringperiod&version=quick");

                        req = WebRequest.Create(url);
                        req.Method = "GET";

                        using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
                            sourceCode = reader.ReadToEnd();

                        // Checks for a valid matchup URL
                        if (sourceCode.Contains("<h1>We're Sorry</h1>"))
                            continue;

                        matchups.Add(matchupCounter, new Matchup(matchupId, year, sourceCode));
                        matchupCounter++; 
                    }

                    csv.AppendLine(string.Format($"{year},Week {matchupId}"));
                    csv.AppendLine(string.Format("Owner 1,Owner 2,Qb,Rb1,Rb2,Wr1,Wr2,Te,Flex,D/ST,K,Total,Result,Opponent"));

                    for (int i = matchupCounter - numberOfTeams; i < matchupCounter; i++)
                        if (matchups.TryGetValue(i, out matchupToWrite))
                            csv.AppendLine(matchupToWrite.ToString());

                    csv.AppendLine();
                    csv.AppendLine();
                }
            }

            File.WriteAllText(@"M:\CSV\LeagueStats.csv", csv.ToString());
        }
    }
}
